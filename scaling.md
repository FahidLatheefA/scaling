# Scaling

## Problem Definition

You joined a new project. The project is going through some performance and scaling issues. After some analysis, the team lead asks you to investigate the possibility of using load balancers and understand vertical/horizontal scaling solutions.

## What is Scaling?

Scaling is a way to address extra workloads with the aid of adjusting our web infrastructure. The increased workload can be from an inflow of customers to a massive volume of simultaneous transactions or anything else that pushes the website beyond its capability.

 in a basic server setup, we have one server running that handles all the `requests` from the users in the web application. As the requests come in, the server gives the `responses` out to the user. As the traffic increases, the lone server may crash after the point application can no longer handle additional requests. The solution to this problem will be either horizontal scaling or vertical scaling.

![server.png](server.png)

## Types of Scaling

There are two basic types of scaling,  Horizontal Scaling and Vertical Scaling. They are similar since they both involve adding computing resources to your infrastructure. However, there are distinct differences between the two in terms of implementation and performance.

### <ins> Horizontal Scaling</ins> <ins> (Scaling Out)</ins>

`Horizontal scaling` (also described as "scaling out") method is scaling with the aid of adding more machines to our pool of resources. Horizontal scaling requires breaking a sequential piece of logic into smaller pieces to be used in `parallel` across multiple machines.

Think of horizontal scaling as a railway system that needs to transport more and more goods every day. To address the demand, you add locomotives to increase your output. 

Here we will add extra servers and split our workload to handle all traffic coming to our web application.

![horizontal.png](horizontal.png)


### <ins> Vertical Scaling</ins> <ins>(Scaling Up)</ins>

`Vertical Scaling`(also described as “scaling up”) refers to scaling by adding more power (e.g. CPU, RAM) to the current machine. Vertical Scaling is easier in implementation because the logic doesn’t need to change. We’re just upgrading the existing server with a server with better specifications.

Going back to our train example, as the quantity of goods grows, so does our single locomotive along with its parts. For instance, we are replacing its engine (CPU in case of a server) with a larger one to offer more horsepower. 

Here,  instead of creating additional copies of our application, we add more resources to super-size it.

![vertical.png](vertical.png)

### <ins> Disadvantages </ins>


Horizontal Scaling | Vertical Scaling
------------ | -------------
 It is necessary to carefully **restructure** our application to distribute the jobs across the machines over the network.| During the process, there will be a significant **downtime** since we are replacing a smaller machine with a larger machine.
It also makes the process of **sharing**, passing or updating data more **costly** since you have to create copies of the data.| At a certain point, the new **server limit** may reach its threshold. We may have to re-upgrade the current machine again.

Now comes an important part, how would the application decide which server-engine should these requests redirect to? The answer to this question is `Load Balancers`.

### <ins> Load Balancers </ins>

A load balancer is a device that acts as a web proxy and distributes network or application traffic throughout several servers. This component knows how to route a request from a client to a server on the backend. The load balancer can handle heavy traffic by acting as a **traffic cop** who routes all the traffic to the servers.

Here is how load balancers work to ensure smooth flow of network traffic:

1. Reads a request from the user.
2. Chooses a server.
3. Forwards the connection to the selected server.

![load-balancer.png](balancer.png)

### <ins> Load Balancer Algorithms </ins>

* Random
   
The load balancer randomly picks a server and send a request there. Over time, if we have enough requests, each server should get the same amount of load.

* Round Robin
  
The load balancer will pick one server at a time. When all the servers are selected, we go back to the first server and continues the order.

* Load Based

Here, the load balancer will pick the server with the lowest traffic and route the requests to the lowest traffic server. When the next request comes in, the load balancer recalculates the loads of the servers and selects the lowest-load server.

### <ins> Summary </ins>

Scaling helps in avoiding web application crashing by reducing the server load. The two methods of Scaling are Horizontal Scaling and Vertical Scaling. Load Balancer acts as a traffic cop by routing the network traffics and avoids server outage. Some of the algorithms used in the load balancer are random, round-robin and load-based algorithms.

### <ins> References and Resources </ins>

1. [Udacity Video](https://www.youtube.com/watch?v=xUumgxZ04SM)

2. [Section Blog](https://www.section.io/blog/scaling-horizontally-vs-vertically/)

3. [Stratoscale Blog](https://www.stratoscale.com/blog/cloud/implementing-horizontal-vertical-scalability-cloud-computing/)

4. [Flowchart Images created using Mermaid Markdown](https://mermaid-js.github.io/mermaid/#/)

```
Fahid Latheef A
August 10, 2021
```
